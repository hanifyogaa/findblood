<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
  protected $table="reviews";
    protected $fillable=[
      'user_id',
      'title',
      'stars',
      'comment'
    ];
  public function User(){
    return $this->belongsTo('App\User','user_id','id');
  }

}
