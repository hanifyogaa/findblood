<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
  protected $table="admins";
    protected $fillable=[
      'user_id',
      'avatar',
    ];

    public function User(){
      return $this->belongsTo('App\User','user_id','id');
    }
}
