<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emergency extends Model
{
  protected $table="emergencies";
    protected $fillable=[
      'user_id',
      'title',
      'picture',
      'description',
      'bloodtype',
      'bloodrhesus',
      'stockneeded',
      'date',
      'time',
      'location',
      'contact',
      'point',
    ];

    public function User(){
      return $this->belongsTo('App\User','user_id','id');
    }

    public function DonationTransactions(){
      return $this->hasMany('App\DonationTransaction','event_id','id');
    }
}
