<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
  protected $table="entities";
    protected $fillable=[
      'user_id',
      'location',
      'avatar',
      'open_days',
      'open_time',
      'close_time'
    ];

    public function User(){
      return $this->belongsTo('App\User','user_id','id');
    }

    public function StokDarah(){
      return $this->hasMany('App\StokDarah','entity_id','id');
    }
}
