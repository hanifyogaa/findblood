<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'point',
        'myreward',
        'bloodtype',
        'bloodrhesus',
        'contact',
        'borndate',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function Event(){
      return $this->hasMany('App\Event','user_id','id');
    }

    public function Emergency(){
      return $this->hasMany('App\Emergency','user_id','id');
    }

    public function Entity(){
      return $this->hasOne('App\Entity','user_id','id');
    }

    public function DonationTransactions(){
      return $this->hasMany('App\DonationTransaction','user_id','id');
    }

    public function News(){
      return $this->hasMany('App\News','user_id','id');
    }

    public function Review(){
      return $this->hasMany('App\Review','user_id','id');
    }

    public function Admin(){
      return $this->hasOne('App\Admin','user_id','id');
    }
}
