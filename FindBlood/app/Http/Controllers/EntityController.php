<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Intervention\Image\ImageServiceProvider;

use App\Admin;
use App\BloodStock;
use App\DonationTransaction;
use App\Emergency;
use App\Entity;
use App\Event;
use App\Partnership;
use App\Reward;
use App\User;

class EntityController extends Controller
{
  public function showEntity($id){
    $auth = Auth::user();
    $entities = Entity::find($id)->get();
    return view('entity.entity',compact(
      'auth','entities'));
  }

  public function createEntity(Request $request){
    $auth = Auth::user();
    $addEntity = new Entity;
      $addEntity->user_id = $request->input('user_id');
      $addEntity->location = $request->input('location');
      // $addEvent->picture = $request->input('picture');
      if($request->hasfile('avatar')){
        $file = $request->file('avatar');
        $ext = $file->getClientOriginalExtension();
        $filename = time().'.'.$ext;
        $file->move('image/entity',$filename);
        $addEntity->avatar = $filename;
      }else{
        $addEntity->avatar='sample.jpg';
      }
      $addEntity->open_days = $request->input('open_days');
      $addEntity->open_time = $request->input('open_time');
      $addEntity->close_time = $request->input('close_time');
      $addEntity->save();
      return redirect('home')->with('status', 'New Entity has been Created!');
  }

  public function updateEntity(){

  }

  public function deleteEntity(){

  }
}
