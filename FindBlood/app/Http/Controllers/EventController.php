<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Intervention\Image\ImageServiceProvider;

use App\Admin;
use App\BloodStock;
use App\DonationTransaction;
use App\Emergency;
use App\Entity;
use App\Event;
use App\Partnership;
use App\Reward;
use App\User;
class EventController extends Controller
{
  public function dataEvent(){
    $user = Auth::user();
    return view('event.create-event',compact('user'));
  }
  public function addEvent(Request $request){
      $auth = Auth::user();
      $addEvent = new Event;
        $addEvent->entity_id = $request->input('entity_id');
        $addEvent->title = $request->input('title');
        // $addEvent->picture = $request->input('picture');
        if($request->hasfile('picture')){
          $file = $request->file('picture');
          $ext = $file->getClientOriginalExtension();
          $filename = time().'.'.$ext;
          $file->move('image/event',$filename);
          $addEvent->picture = $filename;
        }else{
          $addEvent->picture='sample.jpg';
        }
        $addEvent->description = $request->input('description');
        $addEvent->date = $request->input('date');
        $addEvent->time = $request->input('time');
        $addEvent->location = $request->input('location');
        $addEvent->contact = $request->input('contact');
        $addEvent->point = $request->input('point');
        $addEvent->save();
        // return redirect('create/event');
      return redirect('home')->with('status', 'New Event has been Created!');
  }

  public function showList(){
      //ambil semua postingan event
      $auth = Auth::user();
      $events = Event::all();
      return view('event.event',compact (
        'events',
        'auth'
      ));
  }

  public function showListJson(){
    //ambil semua postingan event
    $events = Event::all();
    return response()->json($events, 200);
  }

  public function addEventApi(Request $request){
    $addEvent = new Event;
      $addEvent->entity_id = '1';
      $addEvent->title = $request->input('title');
      $addEvent->description = $request->input('description');
      $addEvent->date = $request->input('date');
      $addEvent->picture='sample.jpg';
      $addEvent->time = '00:00';
      $addEvent->location = $request->input('location');
      $addEvent->contact = 'NONE';
      $addEvent->point = '0';
      $addEvent->save();
      // return redirect('create/event');
      return response()->json($addEvent, 201);
}

  public function detailEvent($id){
    $events = Event::where('id',$id)->get();
    $auth = Auth::user();
    return view('event.event-detail',compact (
      'events',
      'auth'
    ));
  }

  public function manageEvent($id){
    $events = Event::where('id',$id)->get();
    $auth = Auth::user();
    return view('event.manage-event',compact (
      'events',
      'auth'
    ));
  }

  public function updateEvent(Request $request, $id){
    $auth = Auth::user();
    $updateEvent = Event::find($id);
      $updateEvent->entity_id = $request->input('entity_id');
      $updateEvent->title = $request->input('title');
      if($request->hasfile('picture')){
        $file = $request->file('picture');
        $ext = $file->getClientOriginalExtension();
        $filename = time().'.'.$ext;
        $file->move('image/event',$filename);
        $updateEvent->picture = $filename;
      }else{
        $updateEvent->picture='sample.jpg';
      }
      $updateEvent->description = $request->input('description');
      $updateEvent->date = $request->input('date');
      $updateEvent->time = $request->input('time');
      $updateEvent->location = $request->input('location');
      $updateEvent->contact = $request->input('description');
      $updateEvent->point = $request->input('point');
      $updateEvent->save();
      return redirect('home')->with('status', 'Event updated!');
  }

  public function deleteEvent($id){
    $del = Event::find($id);
    $del->delete();
    return redirect('home')->with('status', 'Event Deleted!');
  }

  public function showListJson(){
    //ambil semua postingan event
    $events = Event::all();
    return response()->json($events, 200);
  }

  public function addEventApi(Request $request){
    $addEvent = new Event;
      $addEvent->entity_id = '1';
      $addEvent->title = $request->input('title');
      $addEvent->description = $request->input('description');
      $addEvent->date = $request->input('date');
      $addEvent->picture='sample.jpg';
      $addEvent->time = '00:00';
      $addEvent->location = $request->input('location');
      $addEvent->contact = 'NONE';
      $addEvent->point = '0';
      $addEvent->save();
      // return redirect('create/event');
      return response()->json($addEvent, 201);
  }
  
  public function registerApi(Request $request) {
    $user = User::create([
      'name' => $request->input('email'),
      'email' => $request->input('email'),
      'password' => Hash::make($request->input('password')),
      'bloodtype' => 'B',
      'bloodrhesus' => 'Other',
      'contact' => '12345',
      'borndate' => '1999-11-11']);

      return response()->json($user, 201);
  }

  public function loginApi(Request $request) {
    //Read parameter given from Android
    $email = $request->input('email');
    $password = $request->input('password');

    //Query to DB based on given email
    $User = User::where('email','=',$email)->first();

    $response = new \stdClass();

    //Check password
    if ($User && Hash::check($password, $User->password)) {
      $response->login = 'true';
      $response->message = 'Login success';
    } else {
      $response->login = 'false';
      $response->message = 'Invalid email/password';
    }

    return response()->json($response, 200);
  }

  public function requestEvent(){

  }

  public function manageDetailE(){

  }
}
