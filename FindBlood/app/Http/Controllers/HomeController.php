<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Admin;
use App\BloodStock;
use App\DonationTransaction;
use App\Emergency;
use App\Entity;
use App\Event;
use App\Partnership;
use App\Reward;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $auth = Auth::user();
        $users = User::all();
        $emergencies = Emergency::paginate(5);
        $events = Event::paginate(6);
        $entities = Entity::paginate(6);
        $rewards = Reward::paginate(6);
        if ($auth->role==="admin") {
          return view('admin.admin',compact(
            'auth',
            'users',
            'emergencies',
            'events',
            'entities',
            'rewards'
          ));
        }elseif ($auth->role==="entity") {
          return view('entity.entity',compact(
            'auth',
            'users',
            'emergencies',
            'events',
            'entities',
            'rewards'
          ));
        }
        return view('home',compact(
          'auth',
          'users',
          'emergencies',
          'events',
          'entities',
          'rewards'
        ));
    }

    public function welcome(){
      // tampilan awal welcome (news, emergency/event, review, event, footer)
      $news = News::all();
      $emergencies = Emergency::paginate(3);
      $reviews = Review::all();
      $events = Event::paginate(6);
      $entities = Entity::paginate(6);
      return view('welcome',compact(
        'news',
        'emergencies',
        'reviews',
        'events',
        'entities'
      ));
    }
}
