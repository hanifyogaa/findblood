<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\Hash;

use App\Admin;
use App\BloodStock;
use App\DonationTransaction;
use App\Emergency;
use App\Entity;
use App\Event;
use App\Partnership;
use App\Reward;
use App\User;

class UserController extends Controller
{
  public function showEdit($id){
    $User = User::find($id);
    return view('user.editprofile',compact('User'));
  }

  public function deleteUser($id){
    $del = User::find($id);
    $del->delete();
    return redirect('home')->with('status', 'User Deleted!');
  }

<<<<<<< HEAD
=======
  public function registerApi(Request $request) {
    $user = User::create([
      'name' => $request->input('email'),
      'email' => $request->input('email'),
      'password' => Hash::make($request->input('password')),
      'bloodtype' => 'B',
      'bloodrhesus' => 'Other',
      'contact' => '12345',
      'borndate' => '1999-11-11']);

      return response()->json($user, 201);
  }

  public function loginApi(Request $request) {
    //Read parameter given from Android
    $email = $request->input('email');
    $password = $request->input('password');

    //Query to DB based on given email
    $User = User::where('email','=',$email)->first();
  
    $response = new \stdClass();

    //Check password
    if ($User && Hash::check($password, $User->password)) {
      $response->login = 'true';
      $response->message = 'Login success';
    } else {
      $response->login = 'false';
      $response->message = 'Invalid email/password';
    }
  
    return response()->json($response, 200);
  }
  
>>>>>>> ee88c5b168f975ddbabfbe997f905bda302616d8
  public function updateProfile(Request $request){
    $User = Auth::user();
    $upUser = User::find($User->id);
      $upUser->name = $request->input('name');
      $upUser->bloodtype = $request->input('bloodtype');
      $upUser->bloodrhesus =  $request->input('bloodrhesus');
      $upUser->contact =  $request->input('contact');
      $upUser->borndate =  $request->input('borndate');
      $upUser->save();
    return redirect('home')->with('status', 'Update Success!');
  }
}
