<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Intervention\Image\ImageServiceProvider;

use App\Admin;
use App\BloodStock;
use App\DonationTransaction;
use App\Emergency;
use App\Entity;
use App\Event;
use App\Partnership;
use App\Reward;
use App\User;

class PartnershipController extends Controller
{
  public function showPartnership(){

  }

  public function addPartnership(){

  }

  public function updatePartnership(){

  }

  public function deletePartnership(){

  }
}
