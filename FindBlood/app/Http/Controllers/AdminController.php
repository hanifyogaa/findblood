<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Intervention\Image\ImageServiceProvider;

use App\Admin;
use App\BloodStock;
use App\DonationTransaction;
use App\Emergency;
use App\Entity;
use App\Event;
use App\Partnership;
use App\Reward;
use App\User;

class AdminController extends Controller
{
  public function ShowManageUser($id){
    $User = User::find($id);
    return view('user.manage',compact('User'));
  }

  public function updateProfile(Request $request){
    $upUser = User::find($request->id);
      $upUser->name = $request->input('name');
      $upUser->bloodtype = $request->input('bloodtype');
      $upUser->bloodrhesus =  $request->input('bloodrhesus');
      $upUser->contact =  $request->input('contact');
      $upUser->borndate =  $request->input('borndate');
      $upUser->role = $request->input('role');
      if ($request->role==="entity") {
        $auth = Auth::user();
        $entity = Entity::all();
        $upUser->save();
        return view('entity.create',compact('upUser','auth','entity'));
      }
      $upUser->save();
    return redirect('home')->with('status', 'Update Success!');
  }
}
