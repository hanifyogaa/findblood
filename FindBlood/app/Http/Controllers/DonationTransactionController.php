<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Intervention\Image\ImageServiceProvider;

use App\Admin;
use App\BloodStock;
use App\DonationTransaction;
use App\Emergency;
use App\Entity;
use App\Event;
use App\Partnership;
use App\Reward;
use App\User;

class DonationTransactionController extends Controller
{
    // show registration donatur
    // dipake sama entitiy buat liat yang daftar
    public function showDetail($id){
      $participats = DonationTransaction::where('event_id',$id)->get();
      return view('donation.list-event',compact(
        'participats'
      ));
    }
    // save
    public function saveRegisDonation(Request $request){
      $regDonation = new DonationTransaction;
        $regDonation->user_id = $request->input('user_id');
        $regDonation->event_id = $request->input('event_id');
        $regDonation->status =  $request->input('status');
        $regDonation->save();
      return redirect('home')->with('status', 'Register Success!');
    }

    public function updateRegisDonation(Request $request,$id){
      $upDonation = DonationTransaction::find($id);
        $upDonation->user_id = $request->input('user_id');
        $upDonation->event_id = $request->input('event_id');
        $upDonation->status =  $request->input('status');
        $upDonation->save();
      return redirect('home')->with('status', 'Update Success!');
    }
      // -> page donation list
    // update
      // -> page manage status, redirect
    // delete
      // manage status, redirect
    // delete by request
}
