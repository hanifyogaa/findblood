<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
  protected $table="events";
    protected $fillable=[
      'entity_id',
      'title',
      'picture',
      'description',
      'date',
      'time',
      'location',
      'contact',
      'point'
    ];

    public function User(){
      return $this->belongsTo('App\User','entity_id','id');
    }

    public function DonationTransactions(){
      return $this->hasMany('App\DonationTransaction','event_id','id');
    }
}
