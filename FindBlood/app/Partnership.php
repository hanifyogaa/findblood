<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partnership extends Model
{
  protected $table="partnerships";
    protected $fillable=[
      'part_name',
      'description',
      'part_type'
    ];

    public function Reward(){
      return $this->hasMany('App\Reward','part_id','id');
    }
}
