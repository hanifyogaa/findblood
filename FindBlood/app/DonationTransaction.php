<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationTransaction extends Model
{
  protected $table="donation_transactions";
    protected $fillable=[
      'user_id',
      'event_id',
      'status'
    ];

    public function User(){
      return $this->belongsTo('App\User','user_id','id');
    }

    public function Event(){
      return $this->belongsTo('App\Event','event_id','id');
    }
}
