<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BloodStock extends Model
{
    protected $table="blood_stocks";
      protected $fillable=[
        'entity_id',
        'stock_A',
        'stock_B',
        'stock_AB',
        'stock_O'
      ];

      public function Entity(){
      return $this->belongsTo('App\Entity','entity_id','id');
    }
}
