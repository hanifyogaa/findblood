<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
  protected $table="rewards";
    protected $fillable=[
      'partner_id',
      'title',
      'picture',
      'description',
      'req_point',
      'stock'
    ];

    public function Partnership(){
      return $this->belongsTo('App\Partnership','partner_id','id');
    }
}
