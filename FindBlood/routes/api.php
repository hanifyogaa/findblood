<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Provides service to retrieve events from database. e.g. GET localhost:8000/events is being called from Android
Route::get('/events', 'EventController@showListJson');

//Provide service to create a new event. e.g. POST localhost:80000/events  is being called from Android
Route::post('/events', 'EventController@addEventApi');

<<<<<<< HEAD
//Provide service to login
Route::get('/login', 'UserController@loginApi');

//Provide service to register a user
Route::post('/register', 'UserController@registerApi');
=======
//Provide service to login 
Route::get('/login', 'UserController@loginApi');

//Provide service to register a user 
Route::post('/register', 'UserController@registerApi');
>>>>>>> ee88c5b168f975ddbabfbe997f905bda302616d8
