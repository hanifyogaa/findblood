<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::resource('/event','EventController');
Route::get('home', 'HomeController@index')->name('home');
Route::get('create/event', 'EventController@dataEvent');
Route::post('create/event', 'EventController@addEvent');
Route::get('detail/event/{id}', 'EventController@detailEvent');
Route::get('list/events', 'EventController@showList');
//manage event
Route::get('Manage/Event/{id}','EventController@manageEvent');
Route::post('Manage/Event/','EventController@updateManage');
Route::get('Delete/Event/{id}','EventController@deleteEvent');
//Participant
Route::post('registration/event/','DonationTransactionController@saveRegisDonation');
Route::post('Participant/Updated/{id}','DonationTransactionController@updateRegisDonation');
Route::post('Participant/Deleted','DonationTransactionController@delRegisDonation');
Route::get('Participant/Event/{id}','DonationTransactionController@showDetail');
//Profile
Route::get('Edit/Profile/{id}','UserController@showEdit');
Route::post('Updated/Profile','UserController@updateProfile');
//admin-dashboard
Route::get('Manage/User/{id}','AdminController@ShowManageUser');
Route::post('Updated/Profile/admin/','AdminController@updateProfile');
Route::post('Created/Entity','EntityController@createEntity');
Route::get('Delete/User/{id}','UserController@deleteUser');
//entity
Route::get('entity/{id}','EntityController@showEntity');
Route::get('detail/entity/{id}','EntityController@showEntity');
// Route::post('/likes/{post_id}','PostController@addLike')->name('like');
// Route::get('List/User')
// Route::get('List/Eser')
