@extends('layouts.app')

@section('content')
  <div class="card">
    <form action="{{ action('EventController@addEvent') }}" method="post">
      @csrf
      <div class="form-group">
        <input type="hidden" name="entity_id" value="{{$user->id}}">
        <br><label>Title :</label><br>
        <input type="text" name="title" value="" placeholder="Write Title here ...">
        <br><label>Picture :</label><br>
        <input type="file" name="picture" value="">
        <br><label>Description :</label><br>
        <input type="text" name="description" value="" placeholder="Write Description here ...">
        <br><label>Date :</label><br>
        <input type="date" name="date" value="">
        <br><label>Start at :</label><br>
        <!-- Starting time -->
        <input type="time" name="date" value="">
        <br><label>Location :</label><br>
        <input type="text" name="location" value="">
        <br><label>Contact :</label><br>
        <input type="text" name="contact" value="">
        <br><label>Point :</label><br>
        <input type="number" name="point" value="">
        <button type="submit" name="NewPost">Create</button>
      </div>
    </form>
  </div>
@endsection
