@extends('layouts.app')

@section('content')
@foreach($entities as $entity)
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card">
            <center>
              <div class="card-header">
                <h1>{{$entity->User->name}}</h1>
              </div>
              <br>
                <img src="{{ asset('image/entity/'.$entity->avatar)}}" alt="" height="300px" width="300px">
              </center>
              <div class="card-body">
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Location</label>
                  <div class="col-sm-10">
                    <input class="form-control-plaintext" type="text" readonly name="entity_id" value="{{$entity->location}}">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Open Days</label>
                  <div class="col-sm-10">
                    <input class="form-control-plaintext" type="text" readonly name="entity_id" value="{{$entity->open_days}}">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Open at</label>
                  <div class="col-sm-10">
                    <input class="form-control-plaintext" type="text" readonly name="entity_id" value="{{$entity->open_time}}">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Close at</label>
                  <div class="col-sm-10">
                    <input class="form-control-plaintext" type="text" readonly name="entity_id" value="{{$entity->close_time}}">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Contact</label>
                  <div class="col-sm-10">
                    <input class="form-control-plaintext" type="text" readonly name="entity_id" value="{{$entity->User->contact}}">
                    <input class="form-control-plaintext" type="text" readonly name="entity_id" value="{{$entity->User->email}}">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Blood Stock</label>
                  <div class="col-sm-10">
                    <input class="form-control-plaintext" type="text" readonly name="entity_id" value="A  {{$entity->User->contact}} Bag(s)">
                    <input class="form-control-plaintext" type="text" readonly name="entity_id" value="B  {{$entity->User->contact}} Bag(s)">
                    <input class="form-control-plaintext" type="text" readonly name="entity_id" value="O  {{$entity->User->contact}} Bag(s)">
                    <input class="form-control-plaintext" type="text" readonly name="entity_id" value="AB {{$entity->User->email}} Bag(s)">
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
</div>
@endforeach
@endsection
