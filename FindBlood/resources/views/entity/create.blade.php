@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-head">
            <center>
            <h4>Create a New Entity</h4>
          </div>
          <div class="card-body">

          <form class="" action="{{ action('EntityController@createEntity')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="user_id" value="{{$upUser->id}}">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Location</label>
              <div class="col-sm-10">
                <input class="form-control" type="text" name="location" value="" required>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Avatar</label>
              <div class="col-sm-10">
                <input class="form-control" type="file" name="avatar" required>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Operational Days</label>
              <div class="col-sm-10">
                <select id="open_days" class="form-control" name="open_days" required autocomplete="new-open_days">
                  <option disabled selected value> -- select an option -- </option>
                  <option value="All Days">All Days</option>
                  <option value="Weekdays Only">Weekdays Only</option>
                  <option value="Weekend Only">Weekend Only</option>
                  <option value="Other">Other</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Operational Time</label>
              <div class="col-sm-10">
                <input type="time" name="open_time" value=""> till
                <input type="time" name="close_time" value="">
              </div>
            </div>

              <button class="btn btn-secondary btn-sm btn-block" type="submit" name="updateProfile">NEXT</button>
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
