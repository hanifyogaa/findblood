@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card">
            <div class="card-head row">
              <table>
                @foreach($entities as $entity)
                <tr>
                  <td rowspan="4" colspan="4">
                    <img src="{{ asset('image/entity/'.$entity->avatar)}}" alt="" height="300px" width="300px">
                  </td>
                  <td>Name</td>
                  <td> {{$entity->User->name}} </td>
                </tr>
                <tr>
                  <td>Location</td>
                  <td>{{$entity->location}}</td>
                </tr>
                <tr>
                  <td>Contact</td>
                  <td> {{$entity->User->contact}} </td>
                </tr>
                <tr>
                  <td>Operational</td>
                  <td>{{$entity->open_days}}</td>
                </tr>
                <tr>
                  <td>
                    <button class="btn btn-sm btn-secondary btn-block" onclick="location.href=' {{url('event/create')}} ';">
                      ADD NEW EVENT
                    </button>
                  </td>
                  <td>
                    <button class="btn btn-sm btn-secondary btn-block" onclick="location.href=' {{url('dashboard/entity',$entity->id)}} ';">
                      Dashboard
                    </button>
                  </td>
                  <td>
                    <button class="btn btn-sm btn-secondary btn-block" onclick="location.href='{{ url('Edit/Profile',Auth::user()->id)}};' ">
                      Edit Profile
                    </button>
                  </td>
                  <td>
                    <button class="btn btn-sm btn-secondary btn-block" onclick="location.href='{{ url('update/Stock',Auth::user()->id)}};' ">
                      Update Stock
                    </button>
                  </td>
                  <td>
                    <button class="btn btn-sm btn-secondary btn-block" onclick="location.href='{{ url('view/Stock',Auth::user()->id)}};' ">
                      View Stock
                    </button>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
