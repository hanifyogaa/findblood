@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="bloodtype" class="col-md-4 col-form-label text-md-right">{{ __('Blood Type') }}</label>

                            <div class="col-md-6">
                              <select id="bloodtype" class="form-control" name="bloodtype" required autocomplete="new-bloodtype">
                                <option disabled selected value> -- select an option -- </option>
                                <option value="A">A</option>
                                <option value="AB">AB</option>
                                <option value="B">B</option>
                                <option value="O">O</option>
                              </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="bloodrhesus" class="col-md-4 col-form-label text-md-right">{{ __('Blood Rhesus') }}</label>

                            <div class="col-md-6">
                              <select id="bloodrhesus" class="form-control" name="bloodrhesus" required autocomplete="new-bloodrhesus">
                                <option disabled selected value> -- select an option -- </option>
                                <option value="+">+</option>
                                <option value="-">-</option>
                                <option value="Other">Other</option>
                              </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="bloodtype" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                            <div class="col-md-6">
                              <input type="text" class="form-control" name="contact" required autocomplete="new-contact">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="borndate" class="col-md-4 col-form-label text-md-right">{{ __('Born Date') }}</label>

                            <div class="col-md-6">
                              <input type="date" class="form-control" name="borndate" required autocomplete="new-borndate">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
