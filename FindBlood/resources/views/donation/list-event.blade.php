@extends('layouts.app')

@section('content')
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

  <table>
    <tr>
      <th>ID</th>
      <th>Nama</th>
      <th>Blood Type</th>
      <th>Blood Rhesus</th>
      <th>Status</th>
      <th colspan="2">Action</th>
    </tr>
    @foreach ($participats as $list)
    <tr>
      <td> {{$list->id}} </td>
      <td>{{$list->User->name}}</td>
      <td>{{$list->User->bloodtype}}</td>
      <td>{{$list->User->bloodrhesus}}</td>
      <td>{{$list->status}}</td>
      <td>
        <form class="" action=" {{action('DonationTransactionController@updateRegisDonation',$list->id)}} " method="post">
          @csrf
          <input type="hidden" name="user_id" value=" {{$list->user_id}} ">
          <input type="hidden" name="event_id" value=" {{$list->event_id}} ">
          <select class="" name="status">
            <option value="Requested">Requested</option>
            <option value="Registered">Registered</option>
            <option value="Verified">Verified</option>
            <option value="Completed">Completed</option>
            <option value="Expired">Expired</option>
          </select>
          <button type="submit" name="btn-primary">Update</button>
      </form>
      </td>
      <td>
        <form class="" action="{{ action('DonationTransactionController@delRegisDonation') }}" method="post">

        </form>
      </td>
    </tr>
    @endforeach
  </table>
@endsection
