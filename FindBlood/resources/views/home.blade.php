@extends('layouts.app')

@section('content')
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <!-- Menampilkan card emergency -->
          @foreach($emergencies as $emergecy)
          <div class="card-deck">
            <!-- link ke detail emergencynya -->
            <a href="{{ route('detailemergency',$emergecy->id)}}" style="text-decoration: none;">
              <div class="card">
                <!-- image emergency -->
                <img src="{{ asset('image/emergencies/'.$emergecy->picture)}}" class="card-img-top" alt="$emergecy->picture">
                <div class="card-body">
                  <!-- detail emergency information -->
                  <h5 class="card-title">$emergecy->title</h5>
                  <h6 class="card-text">
                    $emergecy->bloodtype
                    $emergecy->bloodrhesus
                  </h6>
                  <p class="card-text">
                    Location : $emergecy->location
                    Point
                  </p>
                </div>
                <div class="card-footer">
                  <small class="text-muted">
                    Created by : $emergecy->User->name
                    Created Date : $emergecy->date
                  </small>
                </div>
              </div>
            </a>
          </div>
          <br>
          @endforeach

          <!-- Menampilkan card event -->
          Event List
          <div class="row">
          @foreach($events as $event)
          <div class="col-lg-4 col-md-6 mb-4">
            <div class="card h-100">
              <a href="{{ url('detail/event',$event->id)}}" style="text-decoration: none;">
              <div class="card-body">
                <center>
                <img src="{{ asset('image/event/'.$event->picture)}}" class="card-img-top" alt="{{$event->picture}}">
                <div class="card-body">
                  <h5 class="card-title">{{$event->title}} </h5>
                </div>
              </center>
              </div>
              <div class="card-footer">
                <p class="card-text">
                  Location : {{$event->location}}<br>
                  Entity : {{$event->User->name}}<br>
                  Point : {{$event->point}}
                </p><br>
                <small class="text-muted">Last updated <br>{{$event->updated_at}}</small>
              </div>
            </a>
            </div>
          </div>
          @endforeach
          <a href="{{ url('/list/events') }}"style="text-decoration: none;">More Event</a>
          </div>

          <!-- Menampilkan list entity -->
          Entity List
          <div class="row">
          @foreach($entities as $entity)
          <div class="col-lg-4 col-md-6 mb-4">
            <div class="card h-100">
              <a href="{{ url('entity',$entity->id)}}" style="text-decoration: none;">
              <div class="card-body">
                <center>
                <img src="{{ asset('image/event/'.$entity->picture)}}" class="card-img-top" alt="{{$event->picture}}">
                <div class="card-body">
                  <h5 class="card-title">{{$entity->User->name}} </h5>
                </div>
              </center>
              </div>
              <div class="card-footer">
                <p class="card-text">
                  Location : {{$entity->location}}<br>
                  Contact : {{$entity->User->contact}}<br>
                </p><br>
                <small class="text-muted">Last updated <br>{{$event->updated_at}}</small>
              </div>
            </a>
            </div>
          </div>
          @endforeach
          <a href="{{ url('/list/events') }}"style="text-decoration: none;">More Event</a>
          </div>

          <!-- Menampilkan list reward yang tersedia -->
          @foreach($rewards as $reward)
          <div class="card-deck">
            <a href="#">link detailnya
              <div class="card">
                gambarnya
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">title</h5>
                </div>
                <div class="card-footer">
                  <p class="card-text">point, stok</p><br>
                  <small class="text-muted">Partnership</small>
                </div>
              </div>
            </a>
          </div>
          @endforeach
        </div>
    </div>
</div>
@endsection
