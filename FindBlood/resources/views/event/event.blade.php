@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <!-- Menampilkan card event -->
          <div class="row">
          @foreach($events as $event)
          <div class="col-lg-4 col-md-6 mb-4">
            <div class="card h-100">
              <a href="{{ url('detail/event',$event->id)}}" style="text-decoration: none;">
              <div class="card-body">
                <center>
                <img src="{{ asset('image/event/'.$event->picture)}}" class="card-img-top" alt="$event->picture">
                <div class="card-body">
                  <h5 class="card-title">{{$event->title}} </h5>
                </div>
              </center>
              </div>
              <div class="card-footer">
                <p class="card-text">
                  Location : {{$event->location}}<br>
                  Entity : {{$event->User->name}}<br>
                  Point : {{$event->point}}
                </p><br>
                <small class="text-muted">Last updated <br>{{$event->updated_at}}</small>
              </div>
            </a>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
@endsection
