@extends('layouts.app')

@section('content')

@foreach ($events as $event)
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">
            <center>
              Manage Event
            </center>
          </div>

          <div class="card-body">
            <form class="" action="{{ action('EventController@updateManage'),$event->id}}" method="post" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="id" value="{{$event->id}}">


              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Created by</label>
                <div class="col-sm-10">
                  <input class="form-control-plaintext" type="text" readonly name="entity_id" value="{{$event->User->name}}">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Event Title</label>
                <div class="col-sm-10">
                  <input class="form-control" type="text" name="title" value="{{$event->title}}" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Description</label>
                <div class="col-sm-10">
                  <input class="form-control" type="text" name="description" value="{{$event->description}}" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Date</label>
                <div class="col-sm-10">
                  <input class="form-control" type="date" name="date" value="{{$event->date}}" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Time</label>
                <div class="col-sm-10">
                  <input class="form-control" type="time" name="time" value="{{$event->time}}" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Location</label>
                <div class="col-sm-10">
                  <input class="form-control" type="text" name="location" value="{{$event->location}}" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Contact</label>
                <div class="col-sm-10">
                  <input class="form-control" type="number" name="contact" value="{{$event->contact}}" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Point</label>
                <div class="col-sm-10">
                  <input class="form-control" type="number" name="point" value="{{$event->point}}" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Picture</label>
                <div class="col-sm-10">
                  <center>
                    <img src="{{ asset('image/event/'.$event->picture)}}" class="card-img-top" alt="$event->picture">
                  </center>
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" name="picture" id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                  </div>
                </div>
              </div>

              <button type="submit" class="btn btn-primary btn-block" name="updateEvent">UPDATE</button>
            </form>
          </div>
        </div>
      </div>
  </div>
</div>
@endforeach

@endsection
