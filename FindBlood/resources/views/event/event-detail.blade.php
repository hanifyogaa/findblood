@extends('layouts.app')
@section('content')
  <!-- EventController@detailEvent -->
  @foreach ($events as $detail)
    <!-- Big picture -->
    <img src="{{ asset('image/event/'.$detail->picture)}}" class="card-img-top" alt="$detail->picture">
    <!-- Title -->
    Title : {{$detail->title}}<br>
    <!-- Detail -->
    location : {{$detail->location}}<br>
    date : {{$detail->date}}<br>
    start at : {{$detail->time}}<br>
    contact : {{$detail->contact}}<br>
    point : {{$detail->point}}<br>
    @if ($auth->role === "regular")
    <form action="{{ action('DonationTransactionController@saveRegisDonation') }}" method="post">
      @csrf
      <!-- formnya arahin ke route->controllernya -->
      <input type="hidden" name="user_id" value="{{$auth->id}}">
      <input type="hidden" name="event_id" value="{{$detail->id}}">
      <input type="hidden" name="status" value="Requested">
      <!-- Regis bisa langsung pas mencet-->
      <button type="submit" name="RegisEvent">Registrasi</button>
      <!-- Kalo langusng munculin alert -->
    </form>
    @else
    <button type="button" name="DonaturList" onclick="location.href='{{ url('Participant/Event',$detail->id) }}';">Show Participant</button>
    <button type="button" name="manageEvent" onclick="location.href='{{ url('Manage/Event',$detail->id) }}';">Manage Event</button>
    @endif
  @endforeach

  <!-- Optional -->
  <!-- card deck max 3 -->
  <!-- Isinya event lainnya -->
  <!-- foreach(events as more) -->
  <!-- foto -->
  <!-- title -->
  <!-- location -->
  <!-- point -->
@endsection
