@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-body">

          <form class="" action="{{ action('UserController@updateProfile'),$User->id }}" method="post">
            @csrf

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Email</label>
              <div class="col-sm-10">
                <input class="form-control-plaintext" type="email" readonly name="email" value="{{$User->email}}" required>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Name</label>
              <div class="col-sm-10">
                <input class="form-control" type="text" name="name" value="{{$User->name}}" required>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Blood Type</label>
              <div class="col-sm-10">
                <select id="bloodtype" class="form-control" name="bloodtype" required autocomplete="new-bloodtype">
                  <option disabled selected value> -- select an option -- </option>
                  <option value="A">A</option>
                  <option value="AB">AB</option>
                  <option value="B">B</option>
                  <option value="O">O</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Blood Rhesus</label>
              <div class="col-sm-10">
                <select id="bloodrhesus" class="form-control" name="bloodrhesus" required autocomplete="new-bloodrhesus">
                  <option disabled selected value> -- select an option -- </option>
                  <option value="+">+</option>
                  <option value="-">-</option>
                  <option value="Other">Other</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Phone Number</label>
              <div class="col-sm-10">
                <input class="form-control" type="text" name="contact" value="{{$User->contact}}" required>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Born Date</label>
              <div class="col-sm-10">
                <input class="form-control" type="date" name="borndate" value="{{$User->borndate}}" required>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Total Points</label>
              <div class="col-sm-10">
                <input class="form-control-plaintext" type="text" readonly name="point" value="{{$User->point}}" required>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Total Reward</label>
              <div class="col-sm-10">
                <input class="form-control-plaintext" type="text" readonly name="myreward" value="0" required>
              </div>
            </div>
              <button class="btn btn-secondary btn-sm btn-block" type="submit" name="updateProfile">Update</button>
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
