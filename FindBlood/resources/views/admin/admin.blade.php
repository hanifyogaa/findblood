@extends('layouts.app')

@section('content')
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <center>
            <h1>Admin Dashboard</h1>
          </center>
          <table class="table text-center">
            <thead class="thead-dark">
              <tr class="">
                <th>Table Name</th>
                <th>ID</th>
                <th>Name</th>
                <th>Additional Information</th>
                <th colspan="2">Action</th>
              </tr>
            </th>
            <tr>
              <td colspan="6">
                <button class="btn btn-sm btn-light btn-block" onclick="location.href=' {{url('create/event')}} ';">
                  ADD NEW EVENT
                </button>
              </td>
            </tr>
            @foreach ($events as $event)
            <tr>
              <td>Event</td>
              <td>{{$event->id}}</td>
              <td>
                <a href="{{ url('detail/event',$event->id)}}" style="text-decoration: none;">
                {{$event->title}}
              </a>
              </td>
              <td>
                  by : {{$event->User->name}}
              </td>
              <td>
                <button class="btn btn-sm btn-primary btn-block" onclick="location.href=' {{url('Manage/Event',$event->id)}} ';">Manage
              </td>
              <td>
                <button class="btn btn-sm btn-dark btn-block" onclick="location.href='{{url('Delete/Event',$event->id)}}';">Delete
              </td>
            </tr>
            @endforeach

            <tr>
              <td colspan="6">
                NEXT TABLE
              </td>
            </tr>
            @foreach ($users as $user)
            <tr>
              <td>User</td>
              <td>{{$user->id}}</td>
              <td>{{$user->name}}</td>
              <td>Role :{{$user->role}}</td>
              <td>
                <button class="btn btn-sm btn-primary btn-block" onclick="location.href='{{url('Manage/User',$user->id)}}';">Manage
              </td>
              <td>
                <button class="btn btn-sm btn-dark btn-block" onclick="location.href='{{url('Delete/User',$user->id)}}';">Delete
              </td>
            </tr>
            @endforeach

            <tr>
              <td colspan="6">
                NEXT TABLE
              </td>
            </tr>
            @foreach ($entities as $entity)
            <tr>
              <td>Entity</td>
              <td>{{$entity->id}}</td>
              <td>
                <a href="{{ url('detail/entity',$entity->id)}}" style="text-decoration: none;">
                  {{$entity->User->name}}
                </a>
              </td>
              <td>{{$entity->location}}</td>
              <td>
                <button class="btn btn-sm btn-primary btn-block" onclick="location.href='{{url('Manage/User',$entity->User->id)}}';">Manage
              </td>
              <td>
                <button class="btn btn-sm btn-dark btn-block" onclick="location.href='{{url('Delete/Entity',$entity->User->id)}}';">Delete
              </td>
            </tr>
            @endforeach

          </table>
        </div>
      </div>
    </div>

@endsection
