<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entities', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->BigInteger('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users');
          $table->string('location',100);
          $table->string('avatar',100)->default('pmi.jpg');
          $table->string('open_days',100);
          $table->string('open_time',100);
          $table->string('close_time',100);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entities');
    }
}
