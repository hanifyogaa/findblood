<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rewards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->BigInteger('parnter_id')->unsigned();
            $table->foreign('parnter_id')->references('id')->on('partnerships');
            $table->string('title',100);
            $table->string('picture',100);
            $table->string('description',260);
            $table->integer('req_point')->length(10);
            $table->integer('stock')->length(10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rewards');
    }
}
