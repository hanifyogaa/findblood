<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('image')->nullable();
            $table->string('role')->default('regular');
            $table->integer('point')->length(10)->nullable();
            $table->string('myreward')->nullable();
<<<<<<< HEAD
            $table->string('bloodtype');
            $table->string('bloodrhesus');
            $table->string('contact');
            $table->string('borndate');
=======
            $table->string('bloodtype')->nullable();
            $table->string('bloodrhesus')->nullable();
            $table->string('contact')->nullable();
            $table->string('borndate')->nullable();
>>>>>>> ee88c5b168f975ddbabfbe997f905bda302616d8
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
