<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->BigInteger('entity_id')->unsigned();
          $table->foreign('entity_id')->references('id')->on('users');
          $table->string('title',100);
          $table->string('picture',100);
          $table->string('description',100);
          $table->string('date',60);
          $table->string('time',60);
          $table->string('location',100);
          $table->string('contact',60);
          $table->string('point',10);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
